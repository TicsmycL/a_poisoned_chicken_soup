package com.lc.mapper;

import com.lc.pojo.Sentence;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Ticsmyc
 * @package com.lc.mapper
 * @date 2019-12-27 17:29
 */
public interface SentenceMapper {
    /**
     * 随机查一条句子
     * @return
     */
    Sentence selByRandom();

    /**
     * 根据id查询一条句子
     * @param id
     * @return
     */
    @Select("select * from sentence where id=#{id}")
    Sentence selById(int id);

    /**
     * 分页查询 ： limit(start , end)
     * @param start
     * @param offset
     * @return
     */
    List<Sentence> selByPage(int start , int offset);
    /**
     * 查询所有
     * @return
     */
    @Select("select * from sentence")
    List<Sentence> selAll();


    /**
     * 查询句子总数
     * @return
     */
    int selCount();

    /**
     * 插入一个句子
     * @param sentence
     * @return
     */
    int insSentence(Sentence sentence);

    /**
     * 根据ID 从数据库中删除一条句子。
     * @param id
     * @return
     */
    @Delete("delete from sentence where id=#{0}")
    int delById(int id);

}
