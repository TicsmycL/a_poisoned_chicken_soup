package com.lc.service.impl;

import com.lc.mapper.SentenceMapper;
import com.lc.pojo.Sentence;
import com.lc.service.SentenceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author Ticsmyc
 * @package com.lc.service.impl
 * @date 2019-12-27 17:35
 * 业务层实现类
 */
@SuppressWarnings("AlibabaAvoidCallStaticSimpleDateFormat")
@Service
public class SentenceServiceImpl implements SentenceService {
    @Resource
    private SentenceMapper sentenceMapper;
    /**
     * 随机数生成器
     */
    @Resource
    private Random random;

    public static final SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 得到数据库中的句子总数
     * @return
     */
    @Override
    public int getSentenceAmount() {
        return sentenceMapper.selCount();
    }

    /**
     * 根据id获取一个句子
     * @param id
     * @return
     */
    @Override
    public Sentence getOneSentence(int id) {
        return sentenceMapper.selById(id);
    }

    /**
     * 获取随机的句子
     * @return
     */
    @Override
    public Sentence getRandomSentence() {

        return sentenceMapper.selByRandom();
    }

    /**
     * 查询一页句子
     *
     * @param start
     * @param offset
     * @return
     */
    @Override
    public List<Sentence> getOnePageSentence(int start, int offset) {
        return sentenceMapper.selByPage(start,offset);
    }

    /**
     * 添加一个句子
     * @param sentence
     * @return
     */
    @Override
    public int addOneSentence(String sentence) {
        int length =sentence.length();
        String top_char=length>10 ? sentence.substring(0,10):sentence;

        String data = dateFormat.format(new Date());
        Sentence oneSentence=new Sentence();

        oneSentence.setChar_count(length);
        oneSentence.setSentence(sentence);
        oneSentence.setTime(data);
        oneSentence.setTop_char(top_char);

        return sentenceMapper.insSentence(oneSentence);
    }

    /**
     * 获取所有的句子
     * @return
     */
    @Override
    public List<Sentence> getAllSentence() {

        return sentenceMapper.selAll();
    }

    /**
     * 删除一个句子
     * @param id
     * @return
     */
    @Override
    public int delelteOneSentence(int id) {

        return sentenceMapper.delById(id);
    }
}
