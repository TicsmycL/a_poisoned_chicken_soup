package com.lc.service;

import com.lc.pojo.Sentence;

import java.util.List;

/**
 * @author Ticsmyc
 * @package com.lc.service
 * @date 2019-12-27 17:33
 * 业务层接口
 */
public interface SentenceService {

    /**
     * 统计句子总数
     * @return
     */
    public int getSentenceAmount();

    /**
     * 根据id查一个句子
     * @param id
     * @return
     */
    public Sentence getOneSentence(int id );

    /**
     * 随机查一个句子
     * @return
     */
    public Sentence getRandomSentence();

    /**
     * 查询一页句子
     * @param start
     * @param end
     * @return
     */
    public List<Sentence> getOnePageSentence(int start, int offset);

    /**
     * 添加一个句子
     * @param sentence
     * @return
     */
    public int addOneSentence(String sentence);

    /**
     * 查询所有句子
     * @return
     */
    public List<Sentence> getAllSentence();

    /**
     * 根据id删除一条句子
     * @param id
     * @return
     */
    public int delelteOneSentence(int id);

}
