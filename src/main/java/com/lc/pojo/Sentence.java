package com.lc.pojo;

import java.text.SimpleDateFormat;

/**
 * @author Ticsmyc
 * @package com.lc.pojo
 * @date 2019-12-27 17:26
 * Sentence实体类
 */
public class Sentence {

    private int id;
    private String sentence;
    private String top_char;
    private int char_count;
    private String time;

    public Sentence() {
    }

    public Sentence(int id, String sentence, String top_char, int char_count, String time) {
        this.id = id;
        this.sentence = sentence;
        this.top_char = top_char;
        this.char_count = char_count;
        this.time = time;
    }

    @Override
    public String toString() {
        return "Sentence{" +
                "id=" + id +
                ", sentence='" + sentence + '\'' +
                ", top_char='" + top_char + '\'' +
                ", char_count=" + char_count +
                ", time='" + time + '\'' +
                '}';
    }

    public void setTop_char(String top_char) {
        this.top_char = top_char;
    }

    public String getTop_char() {
        return top_char;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public void setChar_count(int char_count) {
        this.char_count = char_count;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public String getSentence() {
        return sentence;
    }

    public int getChar_count() {
        return char_count;
    }

    public String getTime() {
        return time;
    }
}
