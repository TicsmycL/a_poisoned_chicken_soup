package com.lc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lc.pojo.Sentence;
import com.lc.service.impl.SentenceServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Ticsmyc
 * @package com.lc.controller
 * @date 2019-12-27 17:42
 * 控制器！
 */
@Controller
public class SentenceController {
    @Resource
    private ObjectMapper objectMapper;
    @Resource
    private SentenceServiceImpl sentenceServiceImpl;

    Logger logger = Logger.getLogger(this.getClass().toString());

    /**
     * 从数据库获取一个句子， 使用json格式返回数据。
     * @param req
     * @param resp
     * @return
     */
    @RequestMapping(value="show", produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getOneSentence(HttpServletRequest req,HttpServletResponse resp) {
        Sentence sentence =sentenceServiceImpl.getRandomSentence();
        int sentenceAmount=sentenceServiceImpl.getSentenceAmount();
        req.setAttribute("sentence",sentence.getSentence());
        try {
            return objectMapper.writeValueAsString(sentence);
        } catch (JsonProcessingException e) {
            logger.info("转json格式错误");
        }
        return null;
    }

    /**
     * 获取数据库中的句子总数
     * @return
     */
    @RequestMapping(value="count")
    @ResponseBody
    public int getSentenceAmount() {
        int amount=sentenceServiceImpl.getSentenceAmount();
        return amount;
    }

    /**
     * 添加一个句子
     * @param req
     * @return
     */
    @RequestMapping(value="add")
    public String addOneSentence(HttpServletRequest req){
        String sentence = req.getParameter("sentence");
        int res = sentenceServiceImpl.addOneSentence(sentence);
        return "redirect:/index.html";
    }

    /**
     * 获取所有的句子
     * @return
     */
    @RequestMapping(value="showAll", produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getAllSentence(){
        List<Sentence> sentences=sentenceServiceImpl.getAllSentence();
        try {
            return objectMapper.writeValueAsString(sentences);
        } catch (JsonProcessingException e) {
            logger.info("转json格式错误");
        }
        return null;
    }

    /**
     * 根据id删除一条语句。
     * 如果返回1 表示删除成功。 如果返回0 表示删除失败。
     * @param req
     * @return
     */
    @RequestMapping(value="delete")
    @ResponseBody
    public String deleteOneSentence(HttpServletRequest req){
        int id = Integer.parseInt(req.getParameter("id"));
        //拿到id之后去数据库中删除这条语句
        int res = sentenceServiceImpl.delelteOneSentence(id);
        return res+"";
    }

    /**
     * 获取一页内容
     * @param req
     * @return
     */
    @RequestMapping(value="getPage", produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getOnePageSentence(HttpServletRequest req){

        int pageNum = Integer.parseInt(req.getParameter("pageNum"));
        int pageSize = Integer.parseInt(req.getParameter("pageSize"));

        int start = (pageNum-1)*pageSize;
        List<Sentence> res = sentenceServiceImpl.getOnePageSentence(start,pageSize);
        try {
            return objectMapper.writeValueAsString(res);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}


